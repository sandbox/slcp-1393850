This is a port of Trackfield and it associated modules for Drupal 7 that is available on d.o (http://drupal.org/project/trackfield) for Drupal 6. The module was originally created by raintonr (http://drupal.org/user/27877) to whom all praise and lauding should be directed :o) There is currently no custom code or extensions to this D7 version of the module. Any such work will be denoted in comments or a Readme.txt file in the relevant module folders.


Things to know regarding this version of Trackfield for Drupal 7:

-- Trackfield does not currently update the Location data for the Node as it does in D6, a patch for this is welcome.

-- Trackfield graph currently requires the graph configurations form to be saved once after install to assign timestamps and renders to the formatter, otherwise undefined index errors will thrown and the module will not function properly.